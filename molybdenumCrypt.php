<?php

function molybdenumHash($password): string
{

    $mdp_array = str_split($password);

    $last_mdp = '';

    foreach($mdp_array as $key => $element_mdp)
    {
        $ascii_mdp = ord($element_mdp);

        $ascii_mdp += $ascii_mdp;
        $ascii_mdp += $ascii_mdp % 42 ; 

        $last_mdp .= $ascii_mdp;
    }

    $very_last_mdp = strval($last_mdp);

    return $very_last_mdp;
}

$mdp = molybdenumHash('patate');

var_dump($mdp);


